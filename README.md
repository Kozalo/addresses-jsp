Addresses-JSP
=============

It's a laboratory work on the Modern Tools for Software Development for NArFU. This project demonstrates how to write a
server application in Java and run it in Tomcat 8. MySQL is used as an RDBMS, Hibernate for ORM.

You can use the `tomcat7:deploy` Maven goal to deploy the application to the application server. But don't forget to
correct the `src/main/resources/META-INF/persistence.xml` file according to your database configuration.


What does it actually do?
-------------------------

The application is intended to manage geographical objects of different types. They can be created, modified of deleted
completely. The index page just renders the list of such objects as a table.
