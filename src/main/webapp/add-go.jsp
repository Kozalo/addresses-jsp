<%@ page import="java.util.List" %>
<%@ page import="ru.narfu.kozalo.addresses.repositories.GeographicalObjectRepository" %>
<%@ page import="ru.narfu.kozalo.addresses.entities.GeographicalObjectType" %>
<%@ page import="ru.narfu.kozalo.addresses.entities.GeographicalObject" %>
<%@ page import="ru.narfu.kozalo.addresses.repositories.GeographicalObjectTypeRepository" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% List<GeographicalObject> geographicalObjects = new GeographicalObjectRepository().getAll(); %>
<% List<GeographicalObjectType> types = new GeographicalObjectTypeRepository().getAll(); %>

<!DOCTYPE html>
<html>
    <head>
        <title>Добавить географический объект</title>
        <link rel="stylesheet" href="<c:url value="/appearance.css" />">
    </head>
    <body>
        <form method="POST" action="<c:url value="/do/add" />">
            <div>
                <label for="name">Название географического объекта</label>
                <input id="name" name="name" type="text">
            </div>

            <div>
                <label for="type">Тип географического объекта</label>
                <select id="type" name="type" size="1">
                    <c:forEach var="type" items="<%= types %>">
                        <option value="${type.id}">${type.fullName}</option>
                    </c:forEach>
                </select>
            </div>

            <div>
                <label for="parent">Родительский ГО</label>
                <select id="parent" name="parent"> size="1">
                    <option value="0">&lt;нет&gt;</option>
                    <c:forEach var="go" items="<%= geographicalObjects %>">
                        <option value="${go.id}">${go.name}</option>
                    </c:forEach>
                </select>
            </div>

            <div>
                <input type="submit" value="Добавить">
            </div>
        </form>
    </body>
</html>
