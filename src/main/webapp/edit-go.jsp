<%@ page import="java.util.List" %>
<%@ page import="ru.narfu.kozalo.addresses.repositories.GeographicalObjectRepository" %>
<%@ page import="ru.narfu.kozalo.addresses.entities.GeographicalObjectType" %>
<%@ page import="ru.narfu.kozalo.addresses.entities.GeographicalObject" %>
<%@ page import="ru.narfu.kozalo.addresses.repositories.GeographicalObjectTypeRepository" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% GeographicalObjectRepository geographicalObjectRepository = new GeographicalObjectRepository(); %>
<% List<GeographicalObject> geographicalObjects = geographicalObjectRepository.getAll(); %>
<% List<GeographicalObjectType> types = new GeographicalObjectTypeRepository().getAll(); %>

<%
    int id;
    try {
        id = Integer.parseInt(request.getParameter("id"));
    } catch (NumberFormatException e) {
        response.sendError(400);
        return;
    }
    GeographicalObject geographicalObject = geographicalObjectRepository.getById(id);
%>

<!DOCTYPE html>
<html>
    <head>
        <title>Изменить географический объект</title>
        <link rel="stylesheet" href="<c:url value="/appearance.css" />">
    </head>
    <body>
        <form method="POST" action="<c:url value="/do/edit" />">
            <div>
                <label for="id">Идентификатор</label>
                <input id="id" name="id" type="number" value="<%= id %>" readonly>
            </div>

            <div>
                <label for="name">Название географического объекта</label>
                <input id="name" name="name" type="text" value="<%= geographicalObject.getName() %>">
            </div>

            <div>
                <label for="type">Тип географического объекта</label>
                <select id="type" name="type" size="1">
                    <%
                        String template = "<option value=\"%s\" %s>%s</option>";
                        for (GeographicalObjectType type : types) {
                            String isSelected = (type.equals(geographicalObject.getType())) ? "selected" : "";
                            String line = String.format(template, type.getId(), isSelected, type.getFullName());
                            out.println(line);
                        }
                    %>
                </select>
            </div>

            <div>
                <label for="parent">Родительский ГО</label>
                <select id="parent" name="parent"> size="1">
                    <option value="0">&lt;нет&gt;</option>
                    <%
                        template = "<option value=\"%s\" %s>%s</option>";
                        for (GeographicalObject go : geographicalObjects) {
                            if (go.equals(geographicalObject)) {
                                continue;
                            }
                            String isSelected = (go.equals(geographicalObject.getParent())) ? "selected" : "";
                            String line = String.format(template, go.getId(), isSelected, go.getName());
                            out.println(line);
                        }
                    %>
                </select>
            </div>

            <div>
                <input type="submit" value="Изменить">
            </div>
        </form>
    </body>
</html>
