<%@ page import="ru.narfu.kozalo.addresses.repositories.GeographicalObjectRepository" %>
<%@ page import="ru.narfu.kozalo.addresses.entities.GeographicalObject" %>

<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% GeographicalObject geographicalObject = null; %>
<c:catch><%
    int id = Integer.parseInt(request.getPathInfo().substring(1));
    geographicalObject = new GeographicalObjectRepository().getById(id);
%></c:catch>

<!DOCTYPE html>
<html>
    <head>
        <title>Географический объект</title>
    </head>
    <body>
        <% if (geographicalObject != null) { %>
            <% if (geographicalObject.getChildren().isEmpty()) { %>
                <p>У этого географического объекта нет дочерних объектов.</p>
            <% } else { %>
                <ul>
                    <c:forEach var="go" items="<%= geographicalObject.getChildren() %>">
                        <li>
                            <a href="<c:url value='/geographical-object/${go.getId()}' />">
                                <c:choose>
                                    <c:when test="${go.getType().getShortName() != null}">
                                        ${go.getType().getShortName()} ${go.getName()}
                                    </c:when>
                                    <c:otherwise>
                                        ${go.getType().getFullName()} ${go.getName()}
                                    </c:otherwise>
                                </c:choose>
                            </a>
                        </li>
                    </c:forEach>
                </ul>
            <% } %>
        <% } else { %>
            <jsp:forward page="/" />
        <% } %>
    </body>
</html>
