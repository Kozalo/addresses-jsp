<%@ page import="java.util.List" %>
<%@ page import="ru.narfu.kozalo.addresses.repositories.GeographicalObjectTypeRepository" %>
<%@ page import="ru.narfu.kozalo.addresses.entities.GeographicalObjectType" %>
<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% List<GeographicalObjectType> types = new GeographicalObjectTypeRepository().getAll(); %>

<!DOCTYPE html>
<html>
<head>
    <title>Типы географических объектов</title>
    <link rel="stylesheet" href="<c:url value="/appearance.css" />">
</head>
<body>
<table>
    <tr>
        <th>ID</th>
        <th>Название</th>
        <th>Сокращение</th>
    </tr>
    <c:forEach var="type" items="<%= types %>">
        <tr>
            <td>${type.getId()}</td>
            <td>${type.getFullName()}</td>
            <td>${type.getShortName()}</td>
            <td><a href="<c:url value="/edit/type?id=${type.getId()}" />">Изменить</a></td>
            <td><a href="<c:url value="/do/remove/type?id=${type.getId()}" />">Удалить</a></td>
        </tr>
    </c:forEach>
</table>
<br>
<ul>
    <li><a href="<c:url value="/add/type" />">Добавить новый тип географического объекта</a></li>
    <li><a href="<c:url value="/" />">Вернуться на главную страницу</a></li>
</ul>
</body>
</html>
