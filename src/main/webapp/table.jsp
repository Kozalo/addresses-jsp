<%@ page import="ru.narfu.kozalo.addresses.entities.GeographicalObject" %>
<%@ page import="java.util.List" %>
<%@ page import="ru.narfu.kozalo.addresses.repositories.GeographicalObjectRepository" %>
<%@ page contentType="text/html;charset=UTF-8"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% List<GeographicalObject> geographicalObjects = new GeographicalObjectRepository().getAll(); %>

<!DOCTYPE html>
<html>
    <head>
        <title>Географические объекты</title>
        <link rel="stylesheet" href="<c:url value="/appearance.css" />">
    </head>
    <body>
        <table>
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>Тип</th>
                <th>Полный адрес</th>
            </tr>
            <c:forEach var="go" items="<%= geographicalObjects %>">
                <tr>
                    <td>${go.getId()}</td>
                    <td><a href="<c:url value="/geographical-object/${go.getId()}" />">${go.getName()}</a></td>
                    <td><a href="<c:url value="/edit/type?id=${go.getType().getId()}" />">${go.getType().getFullName()}</a></td>
                    <td>${go.getFullAddress()}</td>
                    <td><a href="<c:url value="/edit?id=${go.getId()}" />">Изменить</a></td>
                    <td><a href="<c:url value="/do/remove?id=${go.getId()}" />">Удалить</a></td>
                </tr>
            </c:forEach>
        </table>
        <br>
        <ul>
            <li><a href="<c:url value="/add" />">Добавить новый географический объект</a></li>
            <li><a href="<c:url value="/add/type" />">Добавить новый тип географического объекта</a></li>
            <li><a href="<c:url value="/types" />">Перейти в список типов</a></li>
        </ul>
    </body>
</html>
