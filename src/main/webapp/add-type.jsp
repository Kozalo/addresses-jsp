<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Добавить тип географических объектов</title>
        <link rel="stylesheet" href="<c:url value="/appearance.css" />">
    </head>
    <body>
        <form method="POST" action="<c:url value="/do/add/type" />">
            <div>
                <label for="fullName">Название типа географических объектов</label>
                <input id="fullName" name="fullName" type="text">
            </div>

            <div>
                <label for="shortName">Сокращение</label>
                <input id="shortName" name="shortName" type="text">
            </div>

            <div>
                <input type="submit" value="Добавить">
            </div>
        </form>
    </body>
</html>
