<%@ page import="ru.narfu.kozalo.addresses.entities.GeographicalObjectType" %>
<%@ page import="ru.narfu.kozalo.addresses.repositories.GeographicalObjectTypeRepository" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% GeographicalObjectTypeRepository typeRepository = new GeographicalObjectTypeRepository(); %>

<%
    int id;
    try {
        id = Integer.parseInt(request.getParameter("id"));
    } catch (NumberFormatException e) {
        response.sendError(400);
        return;
    }
    GeographicalObjectType type = typeRepository.getById(id);
%>

<!DOCTYPE html>
<html>
    <head>
        <title>Изменить тип географических объектов</title>
        <link rel="stylesheet" href="<c:url value="/appearance.css" />">
    </head>
    <body>
        <form method="POST" action="<c:url value="/do/edit/type" />">
            <div>
                <label for="id">Идентификатор</label>
                <input id="id" name="id" type="number" value="<%= id %>" readonly>
            </div>

            <div>
                <label for="fullName">Название типа географического объекта</label>
                <input id="fullName" name="fullName" type="text" value="<%= type.getFullName() %>">
            </div>

            <div>
                <label for="shortName">Сокращение</label>
                <input id="shortName" name="shortName" type="text" value="<%= type.getShortNameOrEmpty() %>">
            </div>

            <div>
                <input type="submit" value="Изменить">
            </div>
        </form>
        <br>
        <a href="<c:url value="/do/remove/type?id=" /><%= id %>">Удалить тип</a>
    </body>
</html>
