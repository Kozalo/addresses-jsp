package ru.narfu.kozalo.addresses.servlets;

import ru.narfu.kozalo.addresses.repositories.GeographicalObjectRepository;
import ru.narfu.kozalo.addresses.repositories.GeographicalObjectTypeRepository;


abstract class ManageGeographicalObjectServlet extends BaseManageServlet {

    GeographicalObjectRepository geographicalObjectRepository = new GeographicalObjectRepository();
    GeographicalObjectTypeRepository typeRepository = new GeographicalObjectTypeRepository();

    String parseName() {
        return parseString("name");
    }

    Integer parseTypeId() {
        return tryParseNumber("type");
    }

    Integer parseParentId() {
        return tryParseNumber("parent");
    }

}
