package ru.narfu.kozalo.addresses.servlets;

import ru.narfu.kozalo.addresses.entities.GeographicalObject;
import ru.narfu.kozalo.addresses.entities.GeographicalObjectType;

import javax.servlet.annotation.WebServlet;


@WebServlet(urlPatterns = "/do/add")
public class AddGeographicalObjectServlet extends ManageGeographicalObjectServlet {

    @Override
    void processRequest() {
        String name = parseName();
        Integer typeId = parseTypeId();
        Integer parentId = parseParentId();

        if (name == null || typeId == null || parentId == null) {
            return;
        }

        GeographicalObject geographicalObject = new GeographicalObject();
        geographicalObject.setName(name);
        GeographicalObjectType type = typeRepository.getById(typeId);
        geographicalObject.setType(type);
        if (parentId > 0) {
            GeographicalObject parent = geographicalObjectRepository.getById(parentId);
            geographicalObject.setParent(parent);
        }

        geographicalObjectRepository.persist(geographicalObject);
    }

}
