package ru.narfu.kozalo.addresses.servlets;

import ru.narfu.kozalo.addresses.entities.GeographicalObjectType;

import javax.servlet.annotation.WebServlet;


@WebServlet(urlPatterns = "/do/remove/type")
public class RemoveGeographicalObjectTypeServlet extends ManageGeographicalObjectTypeServlet {

    @Override
    void processRequest() {
        Integer id = parseId();
        if (id == null) {
            return;
        }

        GeographicalObjectType type = typeRepository.getById(id);
        try {
            typeRepository.remove(type);
        } catch (Exception e) {
            produceError("Нельзя удалить тип! Возможно, от него зависит какой-то географический объект?");
        }
    }

}
