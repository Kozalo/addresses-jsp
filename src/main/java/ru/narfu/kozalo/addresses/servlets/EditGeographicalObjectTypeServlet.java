package ru.narfu.kozalo.addresses.servlets;

import ru.narfu.kozalo.addresses.entities.GeographicalObjectType;

import javax.servlet.annotation.WebServlet;


@WebServlet(urlPatterns = "/do/edit/type")
public class EditGeographicalObjectTypeServlet extends ManageGeographicalObjectTypeServlet {

    @Override
    void processRequest() {
        Integer id = parseId();
        String fullName = parseFullName();
        String shortName = parseShortName();
        if (id == null || fullName == null || shortName == null) {
            return;
        }

        GeographicalObjectType type = typeRepository.getById(id);
        type.setFullName(fullName);
        if (!shortName.trim().isEmpty()) {
            type.setShortName(shortName);
        } else {
            type.setShortName(null);
        }

        typeRepository.persist(type);
    }

}
