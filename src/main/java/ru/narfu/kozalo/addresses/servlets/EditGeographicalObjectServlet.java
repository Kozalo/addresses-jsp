package ru.narfu.kozalo.addresses.servlets;

import ru.narfu.kozalo.addresses.entities.GeographicalObject;
import ru.narfu.kozalo.addresses.entities.GeographicalObjectType;

import javax.servlet.annotation.WebServlet;


@WebServlet(urlPatterns = "/do/edit")
public class EditGeographicalObjectServlet extends ManageGeographicalObjectServlet {

    @Override
    void processRequest() {
        Integer id = parseId();
        String name = parseName();
        Integer typeId = parseTypeId();
        Integer parentId = parseParentId();
        if (id == null || name == null || typeId == null || parentId == null) {
            return;
        }

        GeographicalObject geographicalObject = geographicalObjectRepository.getById(id);
        geographicalObject.setName(name);
        GeographicalObjectType type = typeRepository.getById(typeId);
        geographicalObject.setType(type);
        if (parentId > 0) {
            GeographicalObject parent = geographicalObjectRepository.getById(parentId);
            geographicalObject.setParent(parent);
        }

        geographicalObjectRepository.persist(geographicalObject);
    }
}
