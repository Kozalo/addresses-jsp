package ru.narfu.kozalo.addresses.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;


abstract class BaseManageServlet extends HttpServlet {

    abstract void processRequest();

    protected HttpServletRequest request = null;
    protected Logger logger = Logger.getLogger(this.getClass().getName());

    private HttpServletResponse response = null;
    private boolean wasError = false;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        request = req;
        response = resp;
        response.setContentType("text/plain; charset=utf-8");
        processRequest();
        if (!wasError) {
            resp.sendRedirect(request.getContextPath());
        }
    }

    Integer parseId() {
        return tryParseNumber("id");
    }

    Integer tryParseNumber(String fieldName) {
        int number;
        try {
            number = Integer.parseInt(request.getParameter(fieldName));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            wasError = true;
            try {
                showError("Параметр '" + fieldName + "' содержит некорректное значение! Обратитесь к администратору или попробуйте позже.");
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            return null;
        }
        return number;
    }

    String parseString(String fieldName) {
        String param = request.getParameter(fieldName);
        if (param == null) {
            wasError = true;
            try {
                showError("Не передан строковой параметр '" + fieldName + "'!");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        byte[] nameAsBytes = param.getBytes(StandardCharsets.ISO_8859_1);
        return new String(nameAsBytes, StandardCharsets.UTF_8);
    }

    void produceError(String message) {
        wasError = true;
        try {
            showError(message);
        } catch (IOException e) {
            PrintWriter printWriter = new PrintWriter(new StringWriter());
            e.printStackTrace(printWriter);
            logger.log(Level.SEVERE, printWriter.toString());
        }
    }

    private void showError(String message) throws IOException {
        response.getWriter().append(message).append('\n');
    }

}
