package ru.narfu.kozalo.addresses.servlets;

import ru.narfu.kozalo.addresses.entities.GeographicalObjectType;

import javax.servlet.annotation.WebServlet;


@WebServlet(urlPatterns = "/do/add/type")
public class AddGeographicalObjectTypeServlet extends ManageGeographicalObjectTypeServlet {

    @Override
    void processRequest() {
        String fullName = parseFullName();
        String shortName = parseShortName();
        if (fullName == null || shortName == null) {
            return;
        }

        GeographicalObjectType type = new GeographicalObjectType();
        type.setFullName(fullName);
        if (!shortName.trim().isEmpty()) {
            type.setShortName(shortName);
        }

        typeRepository.persist(type);
    }

}
