package ru.narfu.kozalo.addresses.servlets;

import ru.narfu.kozalo.addresses.entities.GeographicalObject;

import javax.servlet.annotation.WebServlet;


@WebServlet(urlPatterns = "/do/remove")
public class RemoveGeographicalObjectServlet extends ManageGeographicalObjectServlet {

    @Override
    void processRequest() {
        Integer id = parseId();
        if (id == null) {
            return;
        }

        GeographicalObject geographicalObject = geographicalObjectRepository.getById(id);
        geographicalObjectRepository.remove(geographicalObject);
    }

}
