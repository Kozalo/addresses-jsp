package ru.narfu.kozalo.addresses.servlets;

import ru.narfu.kozalo.addresses.repositories.GeographicalObjectTypeRepository;


abstract class ManageGeographicalObjectTypeServlet extends BaseManageServlet {

    GeographicalObjectTypeRepository typeRepository = new GeographicalObjectTypeRepository();

    String parseFullName() {
        return parseString("fullName");
    }

    String parseShortName() {
        return parseString("shortName");
    }

}
