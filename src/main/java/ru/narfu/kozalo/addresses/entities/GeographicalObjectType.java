package ru.narfu.kozalo.addresses.entities;

import javax.persistence.*;


@Entity
@Table(name = "Types")
@NamedQueries({
    @NamedQuery(name = "Types.getAll", query = "SELECT t FROM GeographicalObjectType t"),
    @NamedQuery(name = "Types.getById", query = "SELECT t FROM GeographicalObjectType t WHERE t.id = :id")
})
public class GeographicalObjectType {

    private int id;
    private String fullName;
    private String shortName;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Column(name = "full_name", length = 50, nullable = false)
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


    @Column(name = "short_name", length = 10)
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    @Transient
    public String getShortNameOrEmpty() {
        return (shortName != null) ? shortName : "";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;

        GeographicalObjectType that = (GeographicalObjectType) o;

        if (getId() != that.getId()) return false;
        if (!getFullName().equals(that.getFullName())) return false;
        return getShortName() != null ? getShortName().equals(that.getShortName()) : that.getShortName() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getFullName().hashCode();
        result = 31 * result + (getShortName() != null ? getShortName().hashCode() : 0);
        return result;
    }

}
