package ru.narfu.kozalo.addresses.entities;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name = "GeographicalObjects")
@NamedQueries({
    @NamedQuery(name = "GeographicalObject.getAll", query = "SELECT go FROM GeographicalObject go"),
    @NamedQuery(name = "GeographicalObject.getRoot", query = "SELECT go FROM GeographicalObject go WHERE go.parent IS NULL"),
    @NamedQuery(name = "GeographicalObject.getById", query = "SELECT go FROM GeographicalObject go WHERE go.id = :id")
})
public class GeographicalObject {

    private int id;
    private String name;
    private GeographicalObjectType type;
    private GeographicalObject parent;
    private Set<GeographicalObject> children;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Column(name = "name", length = 50, nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type")
    public GeographicalObjectType getType() {
        return type;
    }

    public void setType(GeographicalObjectType type) {
        this.type = type;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent")
    public GeographicalObject getParent() {
        return parent;
    }

    public void setParent(GeographicalObject parent) {
        this.parent = parent;
    }


    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    public Set<GeographicalObject> getChildren() {
        return children;
    }

    public void setChildren(Set<GeographicalObject> children) {
        this.children = children;
    }

    @Transient
    public String getFullName() {
        return (getType().getShortNameOrEmpty() + " " + getName()).trim();
    }

    @Transient
    public String getFullAddress() {
        return fullAddressGetterImpl("");
    }

    private String fullAddressGetterImpl(String str) {
        String appendix = (str.isEmpty()) ? getFullName() : getFullName() + ", " + str;
        if (getParent() == null) {
            return appendix;
        } else {
            return getParent().fullAddressGetterImpl(appendix);
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;

        GeographicalObject that = (GeographicalObject) o;

        if (getId() != that.getId()) return false;
        if (!getName().equals(that.getName())) return false;
        if (!getType().equals(that.getType())) return false;
        return getParent() != null ? getParent().equals(that.getParent()) : that.getParent() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getType().hashCode();
        result = 31 * result + (getParent() != null ? getParent().hashCode() : 0);
        return result;
    }

}
