package ru.narfu.kozalo.addresses.repositories;

import ru.narfu.kozalo.addresses.entities.GeographicalObjectType;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;


public class GeographicalObjectTypeRepository extends ApplicationRepository {

    private EntityManager entityManager = entityManagerFactory.createEntityManager();

    public List<GeographicalObjectType> getAll() {
        return entityManager
                .createNamedQuery("Types.getAll", GeographicalObjectType.class)
                .getResultList();
    }

    public GeographicalObjectType getById(int id) {
        return entityManager
                .createNamedQuery("Types.getById", GeographicalObjectType.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public void persist(GeographicalObjectType type) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(type);
        transaction.commit();
    }

    public void remove(GeographicalObjectType type) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.remove(type);
        transaction.commit();
    }

}
