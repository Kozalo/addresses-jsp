package ru.narfu.kozalo.addresses.repositories;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


abstract class ApplicationRepository {

    static EntityManagerFactory entityManagerFactory = Persistence
            .createEntityManagerFactory("GeographicalObjects");

}
