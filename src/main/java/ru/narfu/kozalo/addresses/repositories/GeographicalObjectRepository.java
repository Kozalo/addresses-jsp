package ru.narfu.kozalo.addresses.repositories;

import ru.narfu.kozalo.addresses.entities.GeographicalObject;

import javax.persistence.*;
import java.util.List;


public class GeographicalObjectRepository extends ApplicationRepository {

    private EntityManager entityManager = entityManagerFactory.createEntityManager();

    public List<GeographicalObject> getAll() {
        return entityManager
                .createNamedQuery("GeographicalObject.getAll", GeographicalObject.class)
                .getResultList();
    }

    public List<GeographicalObject> getRoot() {
        return entityManager
                .createNamedQuery("GeographicalObject.getRoot", GeographicalObject.class)
                .getResultList();
    }

    public GeographicalObject getById(int id) {
        return entityManager
                .createNamedQuery("GeographicalObject.getById", GeographicalObject.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public void persist(GeographicalObject geographicalObject) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(geographicalObject);
        transaction.commit();
    }

    public void remove(GeographicalObject geographicalObject) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.remove(geographicalObject);
        transaction.commit();
    }

}
